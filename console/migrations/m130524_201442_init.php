<?php

use yii\db\Migration;
use common\models\User;
use common\models\Date;
use common\models\Time;

class m130524_201442_init extends Migration
{
    /**
     * @var string
     */
    protected $userTable = '{{%user}}';

    /**
     * @var string
     */
    protected $dateTable = '{{%date}}';

    /**
     * @var string
     */
    protected $timeTable = '{{%time}}';

    /**
     * @var string
     */
    protected $bidTable = '{{%bid}}';

    /**
     *
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->userTable, [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'form_token' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable($this->dateTable, [
            'id' => $this->primaryKey(),
            'date' => $this->date()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'FK_date_user',
            $this->dateTable,
            'user_id',
            $this->userTable,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable($this->timeTable, [
            'id' => $this->primaryKey(),
            'time' => $this->time()->notNull(),
            'date_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'FK_time_date',
            $this->timeTable,
            'date_id',
            $this->dateTable,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable($this->bidTable, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'date' => $this->date()->notNull(),
            'time' => $this->time()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $user = new User([
            'username' => 'username',
            'auth_key' => 'auth_key',
            'password_hash' => 'password_hash',
            'email' => 'email@email.email',
            'form_token' => 'qweqweqwe',
            'status' => User::STATUS_ACTIVE
        ]);
        $user->save(false);

        $date1 = new Date([
            'date' => '2016-08-25',
            'user_id' => $user->id
        ]);
        $date1->save(false);
        $date2 = new Date([
            'date' => '2016-08-26',
            'user_id' => $user->id
        ]);
        $date2->save(false);

        $time1 = new Time([
            'time' => '10:00',
            'date_id' => $date1->id
        ]);
        $time1->save(false);
        $time2 = new Time([
            'time' => '11:00',
            'date_id' => $date1->id
        ]);
        $time2->save(false);
        $time3 = new Time([
            'time' => '12:00',
            'date_id' => $date2->id
        ]);
        $time3->save(false);
        $time4 = new Time([
            'time' => '13:00',
            'date_id' => $date2->id
        ]);
        $time4->save(false);
    }

    /**
     *
     */
    public function down()
    {
        $this->dropTable($this->userTable);
    }
}
