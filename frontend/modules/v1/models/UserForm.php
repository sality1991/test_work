<?php
namespace frontend\modules\v1\models;

use Yii;
use yii\base\Model;
use common\models\Bid;
use common\models\User;
use common\models\Date;
use common\models\Time;

/**
 * Signup form
 */
class UserForm extends Model
{
    public $name;
    public $email;
    public $date;
    public $time;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'trim'],
            [['name', 'email', 'date', 'time'], 'required'],
            [['email'], 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function createBid(User $user)
    {
        /** @var Date $date */
        if (($date = $user->getDates()->where(['id' => $this->date])->one()) === null) {
            $this->addError('date', Yii::t('app', 'Date not found'));
            return false;
        }

        /** @var Time $time */
        if (($time = $date->getTimes()->where(['id' => $this->time])->one()) === null) {
            $this->addError('time', Yii::t('app', 'Time not found'));
            return false;
        }

        $bid = new Bid([
            'name' => $this->name,
            'email' => $this->email,
            'date' => $date->date,
            'time' => $time->time
        ]);

        return $bid->save();
    }
}
