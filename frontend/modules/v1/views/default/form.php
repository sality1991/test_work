<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Date;
use common\models\Time;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

/** @var \common\models\User $user */
/** @var string $token */

$dates = $user->dates;
$times = empty($dates) ? [] : ArrayHelper::map($dates[0]->getTimes()->asArray()->all(), 'id', 'time');
$dates = ArrayHelper::map($dates, 'id', 'dateExt');

?>
<div class="v1-default-form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'name')->textInput(['placeholder' => 'Name'])->label(false) ?>
        <?= $form->field($model, 'email')->textInput(['placeholder' => 'E-mail'])->label(false) ?>
        <?= $form->field($model, 'date')->dropDownList($dates, [
            'ajax-url' => Url::to(['/v1/default/times-list']),
            'token' => $token
        ])->label(false) ?>
        <?= $form->field($model, 'time')->dropDownList($times)->label(false) ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
