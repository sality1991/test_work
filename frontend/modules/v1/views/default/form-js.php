<?php
use yii\helpers\Url;
use yii\helpers\Json;
?>
document.addEventListener('DOMContentLoaded', function() {
    (function () {
        document.getElementById(<?=Json::encode($element) ?>).innerHTML = '<iframe src="<?=Url::to(['/v1/default/form', 'token' => $token], true) ?>" frameborder="no"></iframe>';
    })();
});
