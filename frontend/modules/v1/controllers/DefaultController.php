<?php
namespace frontend\modules\v1\controllers;

use Yii;
use yii\web\Response;
use common\models\Bid;
use common\models\User;
use common\models\Date;
use common\models\Time;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use frontend\modules\v1\models\UserForm;

/**
 * Default controller for the `v1` module
 */
class DefaultController extends Controller
{
    /**
     * @var string
     */
    public $layout = 'form';

    /**
     * @param null $token
     * @return string
     */
    public function actionForm($token = null)
    {
        if ($user = User::find()->where(['form_token' => $token])->one()) {
            $model = new UserForm;

            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->createBid($user)) {
                return $this->render('ok-page');
            }

            return $this->render('form', [
                'model' => $model,
                'user' => $user,
                'token' => $token
            ]);
        }
    }

    /**
     * @param null $token
     * @param null $element
     * @return string
     */
    public function actionFormJs($token = null, $element = null)
    {
        return $this->renderPartial('form-js', [
            'token' => $token,
            'element' => $element
        ]);
    }

    /**
     * @param null $dateId
     * @param null $token
     * @return array
     */
    public function actionTimesList($dateId = null, $token = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var User $user */
        if (($user = User::find()->where(['form_token' => $token])->one()) === null) {
            return [];
        }

        /** @var Date $date */
        if (($date = $user->getDates()->where(['id' => $dateId])->one()) === null) {
            return [];
        }

        return ArrayHelper::map($date->getTimes()->asArray()->all(), 'id', 'time');
    }
}
