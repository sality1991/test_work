$(document).ready(function () {
    $(document).on('change', '#userform-date', function (e) {
        var select = $(this);
        var select2 = $('#userform-time');
        var url = select.attr('ajax-url') + '?token=' + select.attr('token') + '&dateId=' + select.val();
        $.get({
            url: url,
            success: function (data) {
                select2.html('');
                $.each(data, function(index, value) {
                    select2.append('<option value="' + index + '">' + value + '</option>');
                });
            }
        });
    });
});
